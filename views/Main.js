import React, { useState, useEffect } from "react";
import { scrollView, View, Text, ScrollView,FlatList, Alert, Image } from "react-native";

import { Button, StyleSheet } from "react-native";
import SelectDropdown from "react-native-select-dropdown";
import MultiSelect from "react-native-multiple-select";


import { db } from "../database/firebase-config";
import {
  collection,
  getDocs,
  addDoc,
} from "firebase/firestore";



const Main = (props) => {
  const Food = ["Tomato", "Cheese", "Apple", "Salat"];
  const Tools = ["Microwave", "Mixer"];
  // Dummy Data for the MutiSelect
  const items = [
    // name key is must. It is to show the text in front
    { id: 1, name: "Tomato" },
    { id: 2, name: "Cheese" },
    { id: 3, name: "Milk" },
  ];
  const items2 = [
    // name key is must. It is to show the text in front
    { id: 1, name: "Mikrowave" },
    { id: 2, name: "Mixer" },
  ];

  // Data Source for the SearchableDropdown
  const [selectedItems, setSelectedItems] = useState([]);

  const onSelectedItemsChange = (selectedItems) => {
    // Set Selected Items
    setSelectedItems(selectedItems);
  };

  useEffect(() => {
    fetch("https://aboutreact.herokuapp.com/demosearchables.php")
      .then((response) => response.json())
      .then((responseJson) => {
        //Successful response from the API Call
        setServerData(responseJson.results);
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);







//Database
//Gets the recipes from the database when the page reloads and saves them in recipes

  const [recipes, setRecipes] = useState([]);
  const recipesCollectionRef = collection(db, "Rezepte");


  useEffect(() => {
    const getRecipes = async () => {
      const data = await getDocs(recipesCollectionRef);
      setRecipes(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
    };

    getRecipes(); 
  }, []);




//ExactSearch find all Recipes that contain only the ingreedients and tools inserted
//ingredients is a Array of strings
//tools is a Array of strings
//return a Array of Objects with the found recipes
//can be accesed by var.Ingredient[x].name or var.Title
//if no recipe is found a empty Array is returned

  function exactSearch(ingredients,tools){        
  
    let countIngredients = 0;
    let countTools = 0;
    let foundRecipes =[];
    let foundEndRecipes =[];

    for(let x = 1; x<=recipes.length; x++){
      let c = x-1;

      for(let y = 1; y<=ingredients.length;y++){
        let d = y-1;
      
        for(let z = 1; z<=recipes[c].Ingredient.length; z++){
          let a = z-1;
        
          if(ingredients[d] == recipes[c].Ingredient[a].name){
            
            countIngredients = countIngredients + 1;
          }
        }
      }
    
      if(countIngredients == recipes[c].Ingredient.length && countIngredients ==ingredients.length){

        foundRecipes.push(recipes[c])
        countIngredients = 0;
      
      }
      countIngredients = 0;

    }



    for(let x = 1; x<=foundRecipes.length; x++){
      let k = x-1;
      
      for(let y = 1; y<=tools.length;y++){
        let d = y-1;
        
      for(let z = 1; z<=foundRecipes[k].Tools.length; z++){
          let a = z-1;
        
          if(tools[d] == foundRecipes[k].Tools[a]){
            
            countTools = countTools + 1;
          }
        }
      }
      
      if(countTools == foundRecipes[k].Tools.length && countTools ==tools.length){

        foundEndRecipes.push(foundRecipes[k].Title)
        countTools = 0;
                              
      }
      countTools = 0;
    }
   return foundEndRecipes;
  }




//overviewSearch find all Recipes that contain the ingreedients and tools inserted, but not exclusively
//ingredients is a Array of strings
//tools is a Array of strings
//return a Array of Objects with the found recipes
//can be accesed by var.Ingredient[x].name or var.Title
//if no recipe is found a empty Array is returned

  function overviewSearch(ingredients, tools){
  
    let countIngredients = 0;
    let foundRecipes =[];
    let countTools = 0;
    let foundEndRecipes =[];

    for(let x = 1; x<=recipes.length; x++){
      let c = x-1;
    
      for(let y = 1; y<=ingredients.length;y++){
        let d = y-1;
        
        for(let z = 1; z<=recipes[c].Ingredient.length; z++){
          let a = z-1;
      
          if(ingredients[d] == recipes[c].Ingredient[a].name){
            
            countIngredients = countIngredients + 1;
          
          }
        }
      }
      if(countIngredients == ingredients.length){

        foundRecipes.push(recipes[c])
        countIngredients = 0;
      
      }
      countIngredients =0;
    }



    for(let x = 1; x<=foundRecipes.length; x++){
      let k = x-1;
      
      for(let y = 1; y<=tools.length;y++){
        let d = y-1;
        
      for(let z = 1; z<=foundRecipes[k].Tools.length; z++){
          let a = z-1;
        
          if(tools[d] == foundRecipes[k].Tools[a]){
            
            countTools = countTools + 1;
          
          }
        }
      }
      
      if(countTools == tools.length){

        foundEndRecipes.push(foundRecipes[k].Title)
        countTools = 0;
                                      
      }
      countTools = 0;
    }
    return foundEndRecipes;

  }









  return (
    <ScrollView>
      <Text>Select your Food supplies</Text>

      <MultiSelect
        hideTags
        items={items}
        uniqueKey="id"
        onSelectedItemsChange={onSelectedItemsChange}
        selectedItems={selectedItems}
        selectText="Pick Items"
        searchInputPlaceholderText="Search Items..."
        onChangeInput={(text) => console.log(text)}
        tagRemoveIconColor="#CCC"
        tagBorderColor="#CCC"
        tagTextColor="#CCC"
        selectedItemTextColor="#CCC"
        selectedItemIconColor="#CCC"
        itemTextColor="#000"
        displayKey="name"
        searchInputStyle={{ color: "#CCC" }}
        submitButtonColor="#48d22b"
        submitButtonText="Submit"
      />

      <Text>Select your Tools</Text>

      <MultiSelect
        hideTags
        items={items2}
        uniqueKey="id"
        onSelectedItemsChange={onSelectedItemsChange}
        selectedItems={selectedItems}
        selectText="Pick Items"
        searchInputPlaceholderText="Search Items..."
        onChangeInput={(text) => console.log(text)}
        tagRemoveIconColor="#CCC"
        tagBorderColor="#CCC"
        tagTextColor="#CCC"
        selectedItemTextColor="#CCC"
        selectedItemIconColor="#CCC"
        itemTextColor="#000"
        displayKey="name"
        searchInputStyle={{ color: "#CCC" }}
        submitButtonColor="#48d22b"
        submitButtonText="Submit"
      />
      <Button color={"#2596be"} title="Exact Search" />
      <Button color={"#2596be"} title="Overview Search"/>

    </ScrollView>
  );
};

export default Main;

