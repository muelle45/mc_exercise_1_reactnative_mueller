import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import SearchRecipe from "./views/SearchRecipe";
import Main from "./views/Main";

{
  // Idea by https://www.youtube.com/watch?v=VE7J0SA1PRQ&list=LL&index=1&t=346s
}

{
  // Navigation
}
const Stack = createNativeStackNavigator();

function ChangeView() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Main"
        options={{ title: "Recipe App" }}
        component={Main}
      />
      <Stack.Screen
        name="SearchRecipe"
        options={{ title: "Search Recipe" }}
        component={SearchRecipe}
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <ChangeView />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
